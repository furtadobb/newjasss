import parameters
import my_statistics

def call_parameters():
    # Inserting RUN parameters to Output files

    firm_txt = open(parameters.firms_data, 'a')
    firm_txt.write('Parameters %s \nMonth, Firm ID, Total Balance $, Number of employees, Inventory size, Total quantity, LONG, LAT \n' % parameters.my_parameters)
    firm_txt.close()

    agent_txt = open(parameters.agents_data, 'a')
    agent_txt.write('Parameters %s \nMonth, LONG, LAT, Agent ID, Balance $, Age, Years of study, Firm ID, Family, Utility \n' % (parameters.my_parameters))
    agent_txt.close()

    house_txt = open(parameters.houses_data, 'a')
    house_txt.write('Parameters %s \nMonth, House Id, LONG,LAT, Size, Listed price, Family ID \n' % (parameters.my_parameters))
    house_txt.close()

def call_quality(actual_month, region_i, region_x):

    quality_txt = open(parameters.quality_data, 'a')
    quality_txt.write('%s %s %s \n' % (actual_month, region_i, region_x))
    quality_txt.close()

def call_gdp_capita(actual_month, firms, regions):

    GDP_region_capita = my_statistics.Statistics().update_GDP_capita(firms, regions)

    gdp_capita_txt = open(parameters.gdp_percapta_data, 'a')
    gdp_capita_txt.write('%s %s \n' % (actual_month, GDP_region_capita))
    gdp_capita_txt.close()

def call_statistics(actual_month, firms, agents, families, regions):

    price_index = my_statistics.Statistics().update_price(firms)
    GDP_index = my_statistics.Statistics().update_GDP(firms)
    unemployment = my_statistics.Statistics().update_unemployment(agents)
    average_workers = my_statistics.Statistics().calculate_average_workers(firms)
    families_wealth = my_statistics.Statistics().calculate_families_wealth(families)
    firms_wealth = my_statistics.Statistics().calculate_firms_wealth(firms)
    firms_profit = my_statistics.Statistics().calculate_firms_profit(firms)
    GINI_index = my_statistics.Statistics().calculate_GINI(families)
    average_utility = my_statistics.Statistics().calculate_utility(families)


    general_output_txt = open(parameters.general_data, 'a')
    general_output_txt.write('%s %s %s %s %s %s %s %s %s %s \n' % (actual_month, price_index, GDP_index, unemployment,
                                                                   average_workers, families_wealth, firms_wealth, firms_profit,
                                                                   GINI_index, average_utility))
    general_output_txt.close()

def call_monthly_parameters(actual_month, firms, houses, agents,families):

    # OUTPUTs of results for analysis
    firm_txt = open(parameters.firms_data, 'a')

    # old pattern
    for firm in firms:
        firm_txt.write('%d, %d, %d, %d, %s, %d, %f, %f \n' % (actual_month, firm.firm_id,firm.total_balance,
                                                                 firm.num_employees(),firm.num_products() ,firm.get_total_quantity(),
                                                                 firm.address_x,firm.address_y))
    firm_txt.close()


    agent_txt = open(parameters.agents_data, 'a')
    for agent in agents:
       agent_txt.write('%d, %f, %f, %d, %d, %d, %d, %s, %s, %f \n' % (actual_month,agent.address_x, agent.address_y,
                                                                 agent.id, agent.money, agent.age,agent.qualification,
                                                                 agent.firm_id, agent.family_id, agent.utility))
    agent_txt.close()

    house_txt = open(parameters.houses_data, 'a')
    for house in houses:
        house_txt.write('%d, %s, %f, %f, %d, %f, %s \n' % (actual_month, house.house_id,house.address_x,
                                                  house.address_y, house.size, house.price, house.family_id))
    house_txt.close()


def region_index(regions):
    region_txt = open(parameters.region_data, 'a')
    for region in regions:
        region_txt.write('%s %s %s \n' % (region.region, region.index, region.pop))
    region_txt.close()