__author__ = 'B. Furtado'

import SPACE
import parameters

class Household(object):

    def __init__(self, house_id, address_x, address_y, size, price, quality=1, family_id=None, region_id=0):
        self.house_id = house_id
        self.address_x = address_x
        self.address_y = address_y
        self.size = size
        self.price = price
        self.quality = quality
        self.family_id = family_id
        self.region_id = region_id

    def get_house_id(self):
        return self.house_id

    def get_region_id(self):
        return self.region_id

    def get_address(self):
        return self.address_x, self.address_y

    def get_size(self):
        return self.size

    def get_price(self):
        return self.price

    # Setting Address
    def set_region_id(self, uno):
        if uno > 1:
            if self.address_x < 0:
                if self.address_y < 0:
                    self.region_id = 0
                else:
                    self.region_id = 1
            elif self.address_y < 0:
                self.region_id = 2
            elif uno == 4:
                self.region_id = 3
            else:
                if self.address_x < SPACE.SPACE_EAST / 2.:
                    if self.address_y < SPACE.SPACE_NORTH / 2.:
                        self.region_id = 3
                    else:
                        self.region_id = 4
                else:
                    if self.address_y < SPACE.SPACE_NORTH / 2.:
                        self.region_id = 5
                    else:
                        self.region_id = 6
        else:
            self.region_id = 0

    def update_quality(self, regions):
        self.quality = self.size * regions[self.region_id].get_index()

    def update_price(self, regions):
        self.price *= (1 + ((regions[self.region_id].get_index() - regions[self.region_id].get_old_index()) /
                       regions[self.region_id].get_old_index()))

    def get_quality(self):
        return self.quality

    # Family moving changes

    def get_family_id(self):
        return self.family_id

    def add_family(self, family_id):
        self.family_id = family_id

    def empty(self):
        self.family_id = None

    def is_occupied(self):
        if self.family_id is None:
            return False

    def __str__(self):
        return 'House ID %d, Family ID %s, Size %s, Price$ %s, Region %s' % (self.house_id, self.family_id, self.size,
                                                                             self.price, self.region_id)
