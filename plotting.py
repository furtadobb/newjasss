__author__ = 'B. Furtado'
import parameters
import pylab as pl
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import pandas as pd
import numpy as np
# Order of column INPUTS: actual_month, price_index, GDP_index, unemployment, average_workers, families_wealth,
# firms_wealth, firms_profit)

def plot_general():
    dat = pd.read_csv(parameters.general_data,sep=" ", header = None)
    dat.plot(x=[0],y=[1],kind='line',label='Price index')
    plt.xlabel('Months')
    plt.ylabel('Price index (%)')
    plt.title('Evolution of price index, monthly', )
    plt.legend([parameters.my_parameters])
    plt.savefig(parameters.output_data_path + ("/temp_evolution_price_index%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

    dat.plot(x=[0],y=[2],kind='line')
    plt.xlabel('Months')
    plt.ylabel('GDP abs Values')
    plt.title('Evolution of GDP values, monthly')
    plt.savefig(parameters.output_data_path + ("/temp_evolution_of_GDP_values%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

    dat.plot(x=[0],y=[3],kind='line',label='Unemployment (%)')
    plt.xlabel('Months')
    plt.ylabel('Unemployment (%)')
    plt.title('Evolution of Unemployment, monthly')
    plt.savefig(parameters.output_data_path + ("/temp_evolution_of_unemployment%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

    dat.plot(x=[0],y=[4],kind='line')
    plt.xlabel('Months')
    plt.ylabel('Average workers')
    plt.title('Evolution of average numbers of workers per firm, monthly')
    plt.savefig(parameters.output_data_path + ("/temp_evolution_of_number_workers_firms%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

    dat.plot(x=[0],y=[5,6],kind='line', color=('r','b'))
    plt.xlabel('Months')
    plt.ylabel('Wealth (US$)')
    plt.title('Evolution of families\' (red) and firms\' (blue) wealth, monthly')
    plt.legend([parameters.my_parameters])
    plt.savefig(parameters.output_data_path + ("/temp_evolution_of_families_and_firms_profit%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

    dat.plot(x=[0],y=[7],kind='line',color="g")
    plt.xlabel('Months')
    plt.ylabel('Profit (US$)')
    plt.title('Evolution of firms\' profit, monthly')
    plt.legend([parameters.my_parameters])
    plt.savefig(parameters.output_data_path + ("/temp_evolution_of_firms_profit%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

    dat.plot(x=[0],y=[8],kind='line',color='r')
    pl.xlabel('Months')
    pl.ylabel('GINI index (%)')
    pl.title('Evolution of GINI index, monthly')
    pl.legend([parameters.my_parameters])
    plt.savefig(parameters.output_data_path + ("/temp_evolution_of_GINI_index%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

    dat.plot(x=[0],y=[9],kind='line',color='b')
    pl.xlabel('Months')
    pl.ylabel('Average Utility of Families (%)')
    pl.title('Evolution of Av. Utility, monthly')
    pl.legend([parameters.my_parameters])
    plt.savefig(parameters.output_data_path + ("/temp_evolution_of_Utility%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        pl.show()

def plot_quality():
    datalist = [(pl.loadtxt(parameters.quality_data))]
    for data in datalist:
        pl.plot(data[:, 0], data[:, 2], 'b.')
    pl.xlabel('Months')
    pl.ylabel('Quality index')
    pl.title('Evolution of quality index, monthly', )
    pl.legend([parameters.my_parameters])
    plt.savefig(parameters.output_data_path + ("/temp_quality%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        pl.show()

def plot_maps_houses_firms():
    reduction_factor = 200 # bigger the factor the size growth up
    datalist_1 = [(np.loadtxt('_firm.txt'))]
    datalist_2 = [(np.loadtxt('_house.txt'))]
    conv_factor_scale1 = max(np.asmatrix(np.array([(np.loadtxt('_firm.txt'))]))[:,3])/reduction_factor
    datamax1=max(np.asmatrix(np.array([(np.loadtxt('_firm.txt'))]))[:,3])/conv_factor_scale1
    length_out1=int(round((datamax1-0)/(10-1),0))
    scale_f1 = list(range(0,datamax1,length_out1))
    conv_factor_scale2 = max(np.asmatrix(np.array([(np.loadtxt('_house.txt'))]))[:,4])/reduction_factor
    datamax2=max(np.asmatrix(np.array([(np.loadtxt('_house.txt'))]))[:,4])/conv_factor_scale2
    length_out2=int(round((datamax2-0)/(10-1),0))
    scale_f2 = list(range(0,datamax2,length_out2))
    plt.xlim(-11, 11)
    plt.ylim(-11, 11)
    for data in datalist_1:
        plt.scatter(data[:, 0], data[:, 1],s=scale_f1, facecolor='g')
    for data in datalist_2:
        plt.scatter(data[:, 0], data[:, 1],s=scale_f2, facecolor='r' )
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.title('Firms Balance(green) and houses price (red) location', )
    plt.legend('Firms (green) Houses (red)')
    plt.savefig(parameters.output_data_path + ("_map_firms_households%s."%parameters.parameters_names), ext="png", close=False, verbose=True)
    if parameters.show_plots_of_each_simulation == True:
        plt.show()

def plot_maps_agent_firms2():
    reduction_factor = 250 # bigger the factor the size growth up
    datalist_1 = np.matrix(np.loadtxt('_firm.txt'))
    datalist_2 = np.matrix(np.loadtxt('_agent.txt'))
    conv_factor_scale1 = max(np.asmatrix(np.array([(np.loadtxt('_firm.txt'))]))[:,3])/reduction_factor
    datamax1=max(np.asmatrix(np.array([(np.loadtxt('_firm.txt'))]))[:,3])/conv_factor_scale1
    length_out1=int(round((datamax1-0)/(10-1),0))
    scale_f1 = list(range(0,datamax1,length_out1))
    conv_factor_scale2 = max(np.asmatrix(np.array([(np.loadtxt('_agent.txt'))]))[:,3])/reduction_factor
    datamax2=max(np.asmatrix(np.array([(np.loadtxt('_agent.txt'))]))[:,3])/conv_factor_scale2
    length_out2=int(round((datamax2-0)/(10-1),0))
    scale_f2 = list(range(0,datamax2,length_out2))
    start1 = 0
    start2 = 0
    end1 = parameters.TOTAL_FIRMS
    end2 = parameters.TOTAL_AGENTS
    nloops = (np.shape(datalist_1)[0])/parameters.TOTAL_FIRMS
    for i in range(nloops):
        plt.xlim(-11, 11)
        plt.ylim(-11, 11)
        data1 = datalist_1[[list(range(start1,end1))],:]
        start1 = start1+parameters.TOTAL_FIRMS
        end1=end1+parameters.TOTAL_FIRMS
        data2 = datalist_2[[list(range(start2,end2))],:]
        start2 = start2+parameters.TOTAL_AGENTS
        end2=end2+parameters.TOTAL_AGENTS
        for dat1 in data1:
            plt.scatter(dat1[:, 0], dat1[:, 1],s=scale_f1, facecolor='g')
        for dat2 in data2:
            plt.scatter(dat2[:, 0], dat2[:, 1],s=scale_f2, facecolor='r' )
        plt.xlabel('Longitude')
        plt.ylabel('Latitude')
        plt.title('Firms (green) and agent balances (red) location 2', )
        plt.legend('Firms (green) Houses (red)')
        plt.savefig((parameters.output_data_path + "_map_firms_households_month_%d"%i), ext="png", close=False, verbose=True)
        if parameters.show_plots_of_each_simulation == True:
            plt.show()

# selecting two collumns dat1[:][[2,3]]
# selecting two rows dat1[:2:3]
def plot_maps_agent_firms():
    reduction_factor = 250 # bigger the factor the size growth up
    dat1 = pd.read_csv('_firm.txt',sep=" ", header = None)
    dat2 = pd.read_csv('_agent.txt',sep=" ", header = None)
    conv_factor_scale1 = max(dat1[:][3])/reduction_factor
    datamax1=max(dat1[:][3])/conv_factor_scale1
    length_out1=int(round((datamax1-0)/(10-1),0))
    scale_f1 = list(range(0,datamax1,length_out1))
    conv_factor_scale2 = max(dat2[:][3])/reduction_factor
    datamax2=max(dat2[:][3])/conv_factor_scale2
    length_out2=int(round((datamax2-0)/(10-1),0))
    scale_f2 = list(range(0,datamax2,length_out2))
    start1 = 0
    start2 = 0
    end1 = parameters.TOTAL_FIRMS
    end2 = parameters.TOTAL_AGENTS
    nloops = (np.shape(dat1)[0])/parameters.TOTAL_FIRMS
    for i in range(nloops):
        plt.xlim(-11, 11)
        plt.ylim(-11, 11)
        data1 = dat1.iloc[start1:end1,:]
        start1 = start1+parameters.TOTAL_FIRMS
        end1 = end1+parameters.TOTAL_FIRMS
        data2 = dat2.iloc[start2:end2,:]
        start2 = start2+parameters.TOTAL_AGENTS
        end2 = end2+parameters.TOTAL_AGENTS
        plt.scatter(x=data1[0], y=data1[1],s=scale_f1, color='g',label="firms")
        plt.scatter(x=data2[0], y=data2[1],s=scale_f2, color='r',label="agents")
        plt.xlabel('Longitude')
        plt.ylabel('Latitude')
        plt.title('Firms (green) and agent balances (red) location - month %d'%i, )
        plt.legend('Firms (green) Houses (red)')
        plt.savefig((parameters.output_data_path + "_map_firms_households_month_%d"%i), ext="png", close=False, verbose=True)
        if parameters.show_plots_of_each_simulation == True:
            plt.show()