
__author__ = 'B. Furtado'

# Class that collects taxes and applies to ameliorate quality of life
class Govern(object):

    def __init__(self, region, boundaries, index=1, old_index=1, treasure=0, pop=0):
        self.region = region
        # boundaries is a list of 4 coordinates that limit the space WEST, EAST, SOUTH AND NORTH
        self.boundaries = boundaries
        self.index = index
        self.old_index = old_index
        self.treasure = treasure
        self.pop = pop

    # Basics
    def get_region(self):
        return self.region

    def get_index(self):
        return self.index

    def get_old_index(self):
        return self.old_index

    def get_treasure(self):
        return self.treasure

    def get_pop(self):
        return self.pop

    def calculate_pop(self, families):
        dummy_pop = 0
        for family in families:
            if family.get_region_id() == self.region:
                dummy_pop += family.num_members()
        self.pop = dummy_pop

    # Index is updated per capita for current population
    def update_index(self):
        self.old_index = self.index
        if self.pop > 0:
            self.index += self.treasure / self.pop
            self.treasure = 0

    def set_region(self, region):
        self.region = region

    def collect_taxes(self, amount):
        self.treasure += amount

    def __str__(self):
        return 'Region ID. %s, QLI. %s Pop. %s' % (self.region, self.index, self.pop)