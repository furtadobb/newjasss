### ReadMe ###

This README documents whatever steps are necessary to understand and run the model.
To run see: How do I get setup below
But it also explains the main ideas of the model
Version submitted to JASSS (October 16, 2015).
Reviewed for bug (Resubmitted March, 2016).

### Goal ###

To model tax revenues and public services offer in a spatially-explicit context and be able to compare Public Policies

### Methodology ###

Use of ABM of a simple economy where firms and households are spatially located

### Research plan ###

1. Make an incremental model of market (labour, goods,real estate) with spatially explicit firms and households.
2. Calibrate with real data
3. Verify, validate
4. Use the model to make policy analysis

* This code reflects phase 1 of Research plan above 
* Phase 2 is in development

### Algorithms' steps / Schedule ###

* The schedule of the model and the successive calling of functions can be easily followed through TIME_ITERATION
* The main action are as follows: 

1. Cleaning previous output
2. Setting system for 1, 4 or 7 regions (to check different outcomes)
3. Creating artificial regions, families, houses, firms, and agents
4. Allocating agents to families and families to households

* The model runs in days, months, quarters and years and activities are called accordingly

5. Day 0. The model is setup. 
5.1 Each firm creates a product. 
5.2. An initial hiring process is set
5.3 Firms and houses locate themselves within each region 

* Daily activities include only Production

* At the end of first month, and every month
6. Pay employees
7. Families consume, given families' average available money
8. Regional government collect taxes, apply to the region and update quality of life index
9. Firms calculate profits, used for firms' strategy setting
10. Firms update prices, given quantity available in stock
11. If needed, hire or fire employees, using a posting system
12. More qualified agents are matched to firms making higher salary payment
13. A percentage of families look for new housing of better quality                                                                            

* Every quarter, firms save profits for future reference

### Initial values of artificial agents, families, firms, households and regions ###

* Space (-10,10; -10, 10)

### Agents ###
* Age (0, 76)
* Qualification (1, 20)
* Money (20, 200)
* Allocation to family, family to first household

### Households ###
* Size (20, 120)
* Price (5, 50) * size
* Location

### Firms ###

* Capital (20, 400)
* Location

### Main processes (formalized equations in text) ###
# Firms #

* Production: Labour qualification exponentiated to an ALPHA parameter of productivity
* Prices: Updated given an excess or lack of produced quantity. 
* Prices: Parameters for given quantity and percentage increase or decrease
* Sale: Firms sells quantity, given prices and amount put forward by consumer
* Wages: Set as profit percentage on capital, plus qualification of agents
* Hire and Fire: Randomly, on average once every four months, firms make hiring, firing decision based on profits 
* Hiring process puts together either highest qualified worker or closest distance to highest paying firm

### Agents and Families ###

* If of age, look for jobs.
* Family money is equally divided within members of the same family.
* Consume a random amount monthly, varying from 0 to total money,
    exponentiated to a BETA parameter of decaying consumption function
* A percentage of families look for houses within budget plus value of old house, but better quality monthly, 
    part of families move in order to cash in price difference
* Households prices are given by their own attributes plus a spatial-bounding value, 
    given by the quality of life index of the region of the household 

### Government of regions ###

* Government collects percentage of taxes on consumption
* All taxes (per capita) are converted monthly to an increase in Quality of life index, treasure goes back to 0

* Outputs and plotting result are produced on monthly basis 

### Parameters ### 
### Run parameters ###

TOTAL_DAYS = 5040
TOTAL_AGENTS = 1000
TOTAL_FAMILIES = 400
TOTAL_HOUSEHOLDS = 450
TOTAL_FIRMS = 110
NUMBER_REGIONS = 4                          

### Model parameters ###
### Firms ###

### Key parameters ###
ALPHA = .35                                 # Production function, labour with decaying exponent, Alpha for K. [0, 1]
BETA = .6                                   # Consumption function, beta. Decaying consumption on wealth
QUANTITY_TO_CHANGE_PRICES = 200             # Quantity considered exceeding/LACKING in inventory,
                                            # triggers change in prices
MARKUP = .03                                # By how much to reduce/increase prices, given high stock
LABOUR MARKET = .25                         # Frequency to check labor market

### Agents ###
SIZE_MARKET = 10                             # Number of firms to check for lower prices, proximity
CONSUMPTION_SATISFACTION = .01              # Satisfaction to increase utility by amount consumed

### Households ###
PERCENTAGE_CHECK_NEW_LOCATION = 0.05        # Percentage of households pursuing new location

### Government ###
TAX_CONSUMPTION = 0.3                      # Charged from firms, percentage of price sold per one unit

### Future work ###

1.	Intercompany businesses
2.  Commuting
3.  Credit market

### References ###

* Based on Lengnick, 2013. Agent-based  macroeconomics:  A  baseline  model. 
* Journal of Economic Behavior & Organization

### Distinctions from Lengnick, 2013 ###

1. Households are FIXED, but families can move into other households
2. We have either four or one government institution that collects taxes and improves quality of life index
3. Wage is paid to the agent, not on the household. However, consumption is based on families' average money
4. There is no fixed structure for the working-consumption network. Distance is used instead.
5. Different wage mechanism

### What is this repository for? ###

* Still private for ongoing development
* As soon as accepted for publication, we'll open the code

### How do I get set up? ###

1. First, take a look at PARAMETERS and see if you want to change any RUN PARAMETERS or any other MODEL PARAMETER
2. Check the directory to save files changing parameter OUTPUT_PATH
3. Choose number of regions
4. Then, just RUN MAIN.py. 
5. Note, at the moment, a lot of output is being generated to help watch the development

### Who do I talk to? ###

Blind for refereees