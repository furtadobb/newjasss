__author__ = 'B2046470858'

import os
import glob
from timeit import default_timer as timer
import plot_multi_run

start_time_overall = timer()


# creating the function to save the loop index "i" for use that in "plot_sensitivity_analysis.py" module
def save_loop_index():

    if os.path.isfile("FilesforControl/loop_index.txt") is True:
        os.remove("FilesforControl/loop_index.txt")

    parameters_txt = open("FilesforControl/loop_index.txt", 'a')
    parameters_txt.write('%d' % index)
    parameters_txt.close()

# define the number of runs for multi run simulation
number_of_runs = 2


# creating the function to save the number of runs to process
def save_number_of_runs():

    if os.path.isfile("FilesforControl/number_of_runs.txt") is True:
        os.remove("FilesforControl/number_of_runs.txt")

    number_of_runs_txt = open("FilesforControl/number_of_runs.txt", 'a')
    number_of_runs_txt.write('%d' % number_of_runs)
    number_of_runs_txt.close()

# calling the function to save the number of runs
save_number_of_runs()

for index in range(number_of_runs):

    # saving the index value
    save_loop_index()

    # call a main.py module to run
    cmd = "python main.py " + str(index)

    # Actually all the main model using system
    os.system(cmd)

    # load the OUTPUT_PATH
    output_path_multi_run = open("FilesforControl/output_path_set.txt").read().replace('\n', '')

    # generating the control list to rename and delete
    list_temp = glob.glob(os.path.join(output_path_multi_run, "temp*.txt"))
    list_temp_png = glob.glob(os.path.join(output_path_multi_run, "temp*.png"))
    list_temp_csv = glob.glob(os.path.join(output_path_multi_run, "temp*.csv"))

    # renaming the variables one by one
    for ren in list_temp:
        os.rename(ren, os.path.join(output_path_multi_run, 'run_%s_' % index+ren.split('\\temp_')[1]))

    for ten in list_temp_png:
        os.rename(ten, os.path.join(output_path_multi_run, 'run_%s_' % index+ten.split('\\temp_')[1]))

    for ten in list_temp_csv:
        os.rename(ten, os.path.join(output_path_multi_run, 'run_%s_' % index+ten.split('\\temp_')[1]))

# calling the functions to formatt and plot the data
plot_multi_run.general_multi_run_analysis()


# variable to end the time to create the agents
end_time_overall = timer()

# Logical test to define the elapsed time based on the time to define the unit
m, s = divmod((end_time_overall-start_time_overall), 60)
h, m = divmod(m, 60)

print('')
print('########################################################')
print('--------------------------------------------------------')
print("Elapsed time to run all %s Simulations %d hs %02d min %02d sec" % (number_of_runs, h, m, s))
print('--------------------------------------------------------')
print('########################################################')
