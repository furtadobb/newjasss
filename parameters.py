# Contains PARAMETERS

import os
import pandas
import sys
import glob
import random
# System constrain: number of households should be larger than number of families
# System constrain: number of members in a family is determined by the average ratio of inputs #agents #families

# OUTPUT DATA #########################################################################################################
# Change your output directory as necessary
# OUTPUT_PATH = "C:/Users/B/Documents/Modelagem/MyModels/output/"
OUTPUT_PATH = '//STORAGE4/carga/MODELO DINAMICO DE SIMULACAO/Exits_python/JASSS_2/1reg/'

#######################################################################################################################
######## CONTROL PROCESS VARIABLES
# variable to define if is "sensitivity analysis" == True and "NOT" == False
sensitivity_choice = True

# variable to define if is "multi runs simulations" == True and "NOT" == False
multi_run_simulation = False

# variable to define if might delete the "waste" == True and "NOT" == False
delete_tests = True

# variable to define if will be printed the statistics and results == True or not == False
print_statistics_and_results_during_process = True

# variable to define print the plots in the end of each simulation
show_plots_of_each_simulation = False

# keep random seed gerneration fixed. if True random number generator is ramdomless, else NO
keep_random_seed = False
#######################################################################################################################
# check if master output path is created, if False, create a dir
if os.path.exists(OUTPUT_PATH) == False:
    # create a dir as output path
    os.mkdir(OUTPUT_PATH)

if keep_random_seed is False:
    fixed_seed = random.Random(1)
else:
    fixed_seed = random

# logical test to define the options to run
# if sensitivity analysis and multi simulations are False, chech the "Tests" directory and if necessary create it
if sensitivity_choice is False and multi_run_simulation == False:
    if os.path.exists(OUTPUT_PATH+"Tests/") == False:

        os.mkdir(OUTPUT_PATH+"Tests/")

        OUTPUT_PATH = os.path.join(OUTPUT_PATH, "Tests/")

    else:

        OUTPUT_PATH = os.path.join(OUTPUT_PATH, "Tests/")

    output_data_path = OUTPUT_PATH

    if  delete_tests == True:

        list_temp = glob.glob(os.path.join(OUTPUT_PATH, '*.txt'))

        # if the list is empty jump
        if len(list_temp)>0:

            # if the list have some data, delete it
            for det in list_temp:
                os.remove(det)

        list_temp = glob.glob(os.path.join(OUTPUT_PATH, '*.png'))

        # if the list is empty jump
        if len(list_temp)>0:

            # if the list have some data, delete it
            for det in list_temp:
                os.remove(det)
# if any other combination, check the existence and if necessary create the directory and delete the data inside the folder
else:
    if sensitivity_choice is True:

        # check the existence of folder
        if os.path.exists(OUTPUT_PATH + "Sensitivity_analysis/") is False:

            # if false, create it
            os.mkdir(OUTPUT_PATH + "Sensitivity_analysis/")

        # set the correct path to output
        else:

            OUTPUT_PATH = os.path.join(OUTPUT_PATH, "Sensitivity_analysis/")

    if  multi_run_simulation is True:

        if os.path.exists(OUTPUT_PATH + "Multi_Simulation/") is False:

            os.mkdir(OUTPUT_PATH + "Multi_Simulation/")

        OUTPUT_PATH = os.path.join(OUTPUT_PATH, "Multi_Simulation/")

        output_data_path = OUTPUT_PATH

# RUN PARAMETERS ######################################################################################################
TOTAL_DAYS = 5040                        # A good number is 5040. 20 years
TOTAL_AGENTS = 1000                        # We have been trying from 1000 to 10,000
TOTAL_FAMILIES = 400                     # Endogenous average family size. We suggest a 2.5 members per family
TOTAL_HOUSEHOLDS = 440                    # Necessarily higher than number of families
TOTAL_FIRMS = 110                             # Roughly around 11% of total number of agents
NUMBER_REGIONS = 1

# PARAMETERS ##########################################################################################################
# FIRMS
# Key parameters
ALPHA = .2
                                 # Production function, labour with decaying exponent, Alpha for K. [0, 1]
BETA = .6
# Consumption function, beta. Decaying consumption on wealth
QUANTITY_TO_CHANGE_PRICES = 200            # Quantity considered exceeding/LACKING in inventory,
                                            # triggers change in prices
MARKUP = 0.03             # By how much to reduce/increase prices, given high stock

LABOUR_MARKET = 0.25

# AGENTS
SIZE_MARKET = 10
CONSUMPTION_SATISFACTION = 0.01               # Satisfaction to increase utility by amount consumed

# HOUSEHOLDS
PERCENTAGE_CHECK_NEW_LOCATION = 0.005        # Percentage of households pursuing new location

# GOVERNMENT
TAX_CONSUMPTION = .3                      # Charged from firms, percentage of price sold per one unit


# Calling parameters for sensitivity choice ###########################################################################
if sensitivity_choice is True:

    dat_parameters_test = pandas.read_csv("InputData/parameters_limits_process.csv", sep=";", decimal=",", header=None)

    # argv 1 == value of First Loop Index
    arg1 = int(sys.argv[1])

    # argv 2 == value of Selected Parameter
    arg2 = float(sys.argv[2])

    # creating a list of values
    first_val_prm = float(dat_parameters_test.iloc[arg1][1])

    # Renaming the variable as the name of parameter (column 0 of data frame)
    # and the value (position selected in the sequence generated between the lowest and the highest values
    vars()[dat_parameters_test.iloc[arg1][0]] = arg2

    # Printing the parameter and its value
    print('Parameter tested :', dat_parameters_test.iloc[arg1][0], "===== Value :", arg2)
    print('')

    # creating folder to save data based on first collunm "paramter_limits.csv". there are the names of parameters
    if os.path.exists(os.path.join(OUTPUT_PATH , dat_parameters_test.iloc[arg1][0])) is False:
        os.mkdir(os.path.join(OUTPUT_PATH, dat_parameters_test.iloc[arg1][0]))

    else:

        if arg1 == 0 and arg2 == first_val_prm:

            # Running a simulation module
            list_temp = os.listdir(os.path.join(OUTPUT_PATH,'Plots'))
            # If the list is empty, skip it
            if len(list_temp) > 0:
                # If the list has some data, delete it
                for item in list_temp:
                    os.remove(os.path.join(OUTPUT_PATH, 'Plots', item))

    output_data_path = os.path.join(OUTPUT_PATH, dat_parameters_test.iloc[arg1][0])

    if os.path.exists(output_data_path) and arg2 == first_val_prm:

        # Running a simulation module
        list_temp = os.listdir(output_data_path)
        # If the list is empty, skip it
        if len(list_temp) > 0:
            # If the list has some data, delete it
            for item in list_temp:
                os.remove(os.path.join(output_data_path, item))

    # check if exists the folder to save plots, if is FALSE == create
    if os.path.exists(os.path.join(OUTPUT_PATH, 'Plots')) is False:
        print(OUTPUT_PATH)
        os.mkdir(os.path.join(OUTPUT_PATH, 'Plots'))


def save_output_path():

    if os.path.isfile("FilesforControl/output_path_set.txt") is True:
        os.remove("FilesforControl/output_path_set.txt")

    output_txt = open("FilesforControl/output_path_set.txt", 'a')
    output_txt.write('%s' % OUTPUT_PATH)
    output_txt.close()

#def multi_run_to_process(OUTPUT_PATH):
if multi_run_simulation is True:

    # argv 1 == value of Loop Index
    arg1 = int(sys.argv[1])

    # creating the folder to save the plots
    if os.path.exists(os.path.join(OUTPUT_PATH, 'Plots')) is False:
        os.mkdir(os.path.join(OUTPUT_PATH, 'Plots'))

    else:

        ## Running a simulation module
        list_temp = os.listdir(os.path.join(OUTPUT_PATH, 'Plots'))

        # If the list is empty, skip it
        if len(list_temp) > 0:
            # If the list has some data, delete it
            for item in list_temp:
                os.remove(os.path.join(OUTPUT_PATH, 'Plots', item))

    if os.path.exists(OUTPUT_PATH) is True and arg1 == 0:

        ## Running a simulation module
        list_temp = os.listdir(OUTPUT_PATH)
        list_temp = [t for t in list_temp if os.path.isfile(os.path.join(OUTPUT_PATH, t)) is True]

        # If the list is empty, skip it
        if len(list_temp) > 0:
            # If the list has some data, delete it
            for item in list_temp:
                os.remove(os.path.join(OUTPUT_PATH, item))

save_output_path()
########################################################################################################################
# Class to facilitate printing ########################################################################################
class Parameters():
    def __init__(self, QUANTITY_TO_CHANGE_PRICES,MARKUP,
                 ALPHA, BETA, TOTAL_DAYS, TOTAL_AGENTS, TOTAL_FAMILIES,
                 TOTAL_HOUSEHOLDS, TOTAL_FIRMS):
        # Firms
        self.QUANTITY_TO_CHANGE_PRICES = QUANTITY_TO_CHANGE_PRICES
        self.MARKUP = MARKUP
        self.ALPHA = ALPHA
        self.BETA = BETA
        # Utility
        self.CONSUMPTION_SATISFACTION = CONSUMPTION_SATISFACTION
        # Household
        self.PERCENTAGE_CHECK_NEW_LOCATION = PERCENTAGE_CHECK_NEW_LOCATION
        # Government
        self.TAX_CONSUMPTION = TAX_CONSUMPTION
        # Run parameters
        self.TOTAL_DAYS = TOTAL_DAYS
        self.TOTAL_AGENTS = TOTAL_AGENTS
        self.TOTAL_FAMILIES = TOTAL_FAMILIES
        self.TOTAL_HOUSEHOLDS = TOTAL_HOUSEHOLDS
        self.TOTAL_FIRMS = TOTAL_FIRMS
        self.NUMBER_REGIONS = NUMBER_REGIONS
        self.SIZE_MARKET = SIZE_MARKET

    def get_TOTAL_DAYS(self):
        return self.TOTAL_DAYS

    def get_TOTAL_AGENTS(self):
        return self.TOTAL_AGENTS

    def get_TOTAL_FAMILIES(self):
        return self.TOTAL_FAMILIES

    def get_TOTAL_HOUSEHOLDS(self):
        return self.TOTAL_HOUSEHOLDS

    def get_TOTAL_FIRMS(self):
        return self.TOTAL_FIRMS

    def get_QUANTITY_TO_CHANGE_PRICES(self):
        return self.QUANTITY_TO_CHANGE_PRICES

    def get_MARKUP(self):
        return self.MARKUP

    def get_ALPHA(self):
        return self.ALPHA

    def get_BETA(self):
        return self.BETA

    def get_CONSUMPTION_SATISFACTION(self):
        return self.CONSUMPTION_SATISFACTION

    def get_PERCENTAGE_CHECK_NEW_LOCATION(self):
        return self.PERCENTAGE_CHECK_NEW_LOCATION

    def get_TAX_CONSUMPTION(self):
        return self.TAX_CONSUMPTION

    def get_SIZE_MARKET(self):
        return self.SIZE_MARKET

    def get_NUMBER_REGIONS(self):
        return self.NUMBER_REGIONS

    def __str__(self):
        return 'Alpha: %s, Price change perc. %s' % \
               (self.ALPHA, self.MARKUP)

my_parameters = Parameters(QUANTITY_TO_CHANGE_PRICES, MARKUP,
                           ALPHA, BETA, TOTAL_DAYS, TOTAL_AGENTS, TOTAL_FAMILIES,TOTAL_HOUSEHOLDS, TOTAL_FIRMS)

# Defining the path and name of output data based in parameters used to run the model
parameters_names = '_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s' % (TOTAL_DAYS,
                                                                      TOTAL_AGENTS,
                                                                      TOTAL_FAMILIES,
                                                                      TOTAL_HOUSEHOLDS,
                                                                      TOTAL_FIRMS,
                                                                      NUMBER_REGIONS,
                                                                      SIZE_MARKET,
                                                                      ALPHA,
                                                                      BETA,
                                                                      QUANTITY_TO_CHANGE_PRICES,
                                                                      MARKUP,
                                                                      LABOUR_MARKET,
                                                                      CONSUMPTION_SATISFACTION,
                                                                      PERCENTAGE_CHECK_NEW_LOCATION,
                                                                      TAX_CONSUMPTION)

firms_data = os.path.join(output_data_path,  "temp_firm%s.txt" % parameters_names)
agents_data = os.path.join(output_data_path, "temp_agent%s.txt" % parameters_names)
houses_data = os.path.join(output_data_path, "temp_house%s.txt" % parameters_names)
general_data = os.path.join(output_data_path, "temp_general%s.txt" % parameters_names)
quality_data = os.path.join(output_data_path, "temp_quality%s.txt" % parameters_names)
region_data = os.path.join(output_data_path, "temp_region%s.txt" % parameters_names)
gdp_percapta_data = os.path.join(output_data_path, "temp_gdp_percapta%s.txt" % parameters_names)
