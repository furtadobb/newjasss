

# Setup family class
# Relevant to distribute income among family members
# Mobile, as it changes houses when in the housing market

class Family(object):

    def __init__(self, family_id, balance=0, household_id=None, region_id=None, house_price=0, address=None, house=None):
        # Family is a set of agents store in a dictionary keyed by id
        self.family_id = family_id
        self.balance = balance
        self.members = {}
        self.household_id = household_id
        self.region_id = region_id
        self.house_price = house_price
        self.address = address
        self.house = house

    def get_family_id(self):
        return self.family_id

    def add_agent(self, agent):
        # Adds a new agent to the set
        self.members[agent.get_id()] = agent
        agent.set_family(self.family_id)

    # Household operations ###########################################################################################

    def get_household(self):
        return self.household_id

    def get_region_id(self):
        return self.region_id

    def assign_house(self, house):
        self.household_id = house.get_house_id()
        self.region_id = house.get_region_id()
        self.house = house

    def empty_house(self):
        house = self.house
        house.empty()
        self.region_id = None

    def set_house_value(self, house):
        self.house_price = house.get_price()

    def get_house_value(self):
        return self.house_price

    def set_address(self, house):
        self.address = house.get_address()
        for key in list(self.members.keys()):
           self.members[key].set_address(house.get_address())

    def get_address(self):
        return self.address

    # Budget operations ##############################################################################################
    def sum_balance(self):
        # Calculates the total available balance of the family
        self.balance = 0
        for key in self.members:
            self.balance += self.members[key].get_money()
        return self.balance

    def update_balance(self, amount):
        if len(self.members) > 0:
            per_member = amount / len(self.members)
            for key in self.members:
                self.members[key].update_money(per_member)

    def average_study(self):
        # Averages the years of study of the family
        dummy_temp = 0
        for key in self.members:
            dummy_temp += (self.members[key].get_qual())
        if self.num_members() > 0:
            self.study = float(dummy_temp) / float(self.num_members())
        else:
            self.study = dummy_temp
        return self.study

    # Families' utility
    def average_utility(self):
        if len(self.members) > 0:
            dummy_utility = 0
            for key in self.members:
                dummy_utility += self.members[key].get_utility()
            dummy_utility2 = dummy_utility / len(self.members)
            return dummy_utility2

    # Basics
    def get_agents(self):
        return list(self.members.values())

    def get_balance(self):
        return self.balance

    def get_agent(self, id):
        "Returns an agent given the agent's ID"
        return self.members[id]

    def is_member(self, id):
        # Returns true if agent is a member of this set"
        return id in self.members

    def num_members(self):
        return len(self.members)

    def __str__(self):
        return 'Family ID %s, House ID %s, $ %s' % (self.family_id, self.household_id, self.balance)
