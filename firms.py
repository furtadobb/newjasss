
import products
import parameters
import SPACE

class Firm(object):
    def __init__(self, firm_id,  address_x, address_y, total_balance, last_qtr_balance=0, profit=1,
                 sum_qualification=0, amount_sold=0,  product_index=0, region=0):

        #amount_produced=0

        self.firm_id = firm_id
        self.address_x = address_x
        self.address_y = address_y
        self.total_balance = total_balance
        self.last_qtr_balance = last_qtr_balance
        self.profit = profit
        # Pool of workers in a given firm
        self.employees = {}
        self.sum_qualification = sum_qualification
        # Firms makes existing products from class Products.
        # Products produced are stored by product_id in the inventory
        self.inventory = {}
        self.sold = amount_sold
        self.product_index = product_index
        self.region_id = region
        #self.amount_produced = amount_produced

    # Firm basics #####################################################################################################
    def get_firm_id(self):
        return self.firm_id

    def get_address(self):
        return self.address_x, self.address_y

    def get_total_balance(self):
        return self.total_balance

    def get_region_id(self):
        return self.region_id

    # Setting ADDRESS
    def set_region_id(self, uno):
        if uno > 1:
            if self.address_x < 0:
                if self.address_y < 0:
                    self.region_id = 0
                else:
                    self.region_id = 1
            elif self.address_y < 0:
                self.region_id = 2
            elif uno == 4:
                self.region_id = 3
            else:
                if self.address_x < SPACE.SPACE_EAST / 2.:
                    if self.address_y < SPACE.SPACE_NORTH / 2.:
                        self.region_id = 3
                    else:
                        self.region_id = 4
                else:
                    if self.address_y < SPACE.SPACE_NORTH / 2.:
                        self.region_id = 5
                    else:
                        self.region_id = 6
        else:
            self.region_id = 0

    # Profits  #######################################################################################################
    # Save value in time, every quarter

    def set_last_qtr(self):
        self.last_qtr_balance = self.total_balance

    def calculate_profit(self):
        self.profit = self.total_balance - self.last_qtr_balance

    def get_profit(self):
        return self.profit

    # Product procedures ##############################################################################################
    # Beginning of month procedure. Check for and creating new products
    # Create product only if firms' balance is positive

    def create_product(self):
        if self.profit > 0:
            dummy_quantity = 0
            dummy_price = 1
            if self.product_index not in list(self.inventory.keys()):
                self.inventory[self.product_index] = products.Product(self.product_index, dummy_quantity, dummy_price)
                self.product_index += 1

    def update_product_quantity(self):
        # Production equation = Labour * qualification
        if len(self.employees) > 0 and self.total_balance > 0 and len(self.inventory) > 0:
            dummy_quantity = self.get_sum_qualification()
            for key in list(self.inventory.keys()):
                self.inventory[key].quantity += dummy_quantity
                #self.amount_produced += dummy_quantity

    def get_total_quantity(self):
        total_quantity = 0
        for key in list(self.inventory.keys()):
            total_quantity += self.inventory[key].quantity
        return total_quantity


    ##################################

    # Updating prices #################################################################################################
    def update_prices(self):
        # If demand is right, maintain price. Low demand, lower prices. High demand, increase prices
        for key in self.inventory.keys():
            if self.inventory[key].quantity > parameters.QUANTITY_TO_CHANGE_PRICES:
                self.inventory[key].price = 1 # BLINDER, 1994
            else:
                self.inventory[key].price *= (1 + parameters.MARKUP)

    def get_prices(self):
        dummy_prices = 0
        for key in list(self.inventory.keys()):
            dummy_prices += self.inventory[key].price
        return dummy_prices / len(self.inventory)

    # Sales ##########################################################################################################
    def sale(self, amount, regions):
        # Selling function
        if amount > 0:
            # For each product in this firms' inventory, spend amount proportionally
            amount_per_product = amount / len(self.inventory)
            # Add price of the unit, deduce it from consumers' amount
            for key in list(self.inventory.keys()):
                if self.inventory[key].quantity > 0:
                    bought_quantity = amount_per_product / self.inventory[key].price

                    # Verifying if demand is within firms' available inventory
                    if bought_quantity > self.inventory[key].quantity:
                        bought_quantity = self.inventory[key].quantity
                        amount_per_product = bought_quantity * self.inventory[key].price

                    # Deducing  from stock
                    self.inventory[key].quantity -= bought_quantity

                    # Tax deducted from firms' balance and value of sale added to the firm
                    self.total_balance += (amount_per_product - (amount_per_product * parameters.TAX_CONSUMPTION))

                    # Tax added to region-specific government
                    regions[self.region_id].collect_taxes(amount_per_product * parameters.TAX_CONSUMPTION)
                    # Quantifying quantity sold
                    self.sold += amount_per_product

                    # Deducing money from clients upfront
                    amount -= amount_per_product

        # Return any change
        return amount

    def num_products(self):
        return len(self.inventory)

    def get_sold(self):
        return self.sold

    # Employees' procedures ##########################################################################################
    def add_employee(self, employee):
        # Adds a new employee to firms' set
        # Employees are instances of Agents
        self.employees[employee.get_id()] = employee
        employee.set_workplace(self.firm_id)

    def fire(self):
        if len(self.employees) > 0:
            key = parameters.fixed_seed.choice(list(self.employees.keys()))
            self.employees[key].set_workplace(None)
            del self.employees[key]

    def is_worker(self, id):
        # Returns true if agent is a member of this firm
        return id in self.employees

    def num_employees(self):
        return len(self.employees)

    def get_sum_qualification(self):
        self.sum_qualification = 0
        for key in list(self.employees.keys()):
            self.sum_qualification += self.employees[key].get_qual() ** parameters.ALPHA
        return self.sum_qualification

    def set_wage_base(self):
        # Based on profitability of the firm (capital)
        # if self.profit > 0 and self.total_balance > 0:
        #     return 1 + self.profit / self.total_balance
        # else:
        return .65

    def make_payment(self):
        if self.num_employees() > 0:
            for key in list(self.employees.keys()):
                # Making payment different to employee given by qualification.
                # Deducing it from firms' balance
                # Adjusting for size of the firm
                self.employees[key].money += (self.set_wage_base()) * 1 * (self.employees[key].get_qual() ** parameters.ALPHA)
                self.total_balance -= (self.set_wage_base()) * 1 * (self.employees[key].get_qual() ** parameters.ALPHA)

    def __str__(self):
        return 'FirmID: %d, $ %d, Emp. %d, Quant. %d, Address: %f, %f' % (self.firm_id, self.total_balance,
                                                                          len(self.employees), self.get_total_quantity(),
                                                                          self.address_x,self.address_y)