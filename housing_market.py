__author__ = 'B. Furtado'

# ALLOCATION OF HOUSES ON THE MARKET #######################################################################

import my_statistics
import random
import parameters

def allocate_houses(families, houses, regions):

    # Select sample of families looking for houses at this time, given parameter
    family_on_the_look = parameters.fixed_seed.sample(families, int(len(families) * parameters.PERCENTAGE_CHECK_NEW_LOCATION))
    m = {}
    # Select available houses, not_occupied and update values
    for on_the_market in houses:
        # Update prices and quality for all houses
        on_the_market.update_price(regions)
        on_the_market.update_quality(regions)
        if on_the_market.is_occupied() is False:
            m[on_the_market.get_house_id()] = on_the_market

    mediana = my_statistics.Statistics().calculate_families_median_wealth(families)
    # For each family

    for family in family_on_the_look:
        # Given the endogenous formation of families, some families may contain no members, excluding those

        family.set_house_value(family.house)
        if family.num_members() > 0:
            move = False
            if family.sum_balance() < mediana:
                # Compare houses that are available, chooses cheapest
                key1 = parameters.fixed_seed.choice(list(m.keys()))
                for key2 in list(m.keys()):
                    if m[key2].get_price() < m[key1].get_price():
                        open_house = m[key2]
                    else:
                        open_house = m[key1]
                # Check if gains are positive
                if family.get_house_value() > open_house.get_price():
                    # Deduce house price difference
                    price_difference = (family.get_house_value() - open_house.get_price())

                    family.update_balance(price_difference)
                    move = True

            else:
                # Compare houses that are available, chooses best
                key1 = parameters.fixed_seed.choice(list(m.keys()))
                for key2 in list(m.keys()):

                    if m[key2].get_quality() > m[key1].get_quality():
                        open_house = m[key2]
                    else:
                        open_house = m[key1]

                # Check availability of funds
                if (family.sum_balance() + family.get_house_value()) > open_house.get_price():
                    # Deduce house price difference
                    price_difference = (family.get_house_value() - open_house.get_price())

                    family.update_balance(price_difference)
                    move = True

            # Make the move
            if move is True:

                # Move out
                family.empty_house()
                # Assign house id to family
                family.assign_house(open_house)
                # Assign family id to house
                open_house.add_family(family.get_family_id())
                family.set_address(open_house)
                family.set_house_value(open_house)
