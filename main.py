import random
import os
import plotting
import parameters
import glob
import generator

parameters.fixed_seed.seed(0)

# Deleting output files
list_temp = glob.glob("*.txt")
for det in list_temp:
    os.remove(det)

# Initialization and setting up
# initialize agents
my_agents = generator.create_agent()

# Initialize regions
if parameters.NUMBER_REGIONS == 4:
    my_regions = generator.create_regions()
elif parameters.NUMBER_REGIONS == 7:
    my_regions = generator.create_seven()
else:
    my_regions = generator.create_uno()

# initialize families
my_families = generator.create_family()

# initialize households
my_houses = generator.create_household()

# initialize firms
my_firms = generator.create_firm()

# Setup REGIONS
for firm in my_firms:
    if parameters.NUMBER_REGIONS == 4:
        firm.set_region_id(4)
    elif parameters.NUMBER_REGIONS == 7:
        firm.set_region_id(7)
    else:
        firm.set_region_id(1)

for house in my_houses:
    if parameters.NUMBER_REGIONS == 4:
        house.set_region_id(4)
    elif parameters.NUMBER_REGIONS == 7:
        house.set_region_id(7)
    else:
        house.set_region_id(1)

# allocate agents to families
def allocate_to_family(agents, families):
    dummy_num_agents = len(agents)
    while dummy_num_agents > 0:
        agent = parameters.fixed_seed.choice(agents)
        family = parameters.fixed_seed.choice(families)
        if agent.belongs_to_family() is False:
            family.add_agent(agent)
            dummy_num_agents -= 1

allocate_to_family(my_agents, my_families)

# allocate families to households
def allocate_to_households(families, households):
    for family in families:
        dummy = 1
        while dummy > 0:
            house = parameters.fixed_seed.choice(households)
            if house.is_occupied() is False:
                family.assign_house(house)
                house.add_family(family.get_family_id())
                family.set_address(house)
                family.set_house_value(house)
                dummy -= 1

allocate_to_households(my_families, my_houses)

# Printing and Testing
for family in my_families:
    print(family)
    for key in list(family.members.keys()):
        print((family.members[key]))

print('')
print('Basic agents, families, household, firms setup.')

# initializing time sequence
generator.have_a_go(my_firms, my_agents, my_families, my_houses, my_regions)

# Initializing PLOTs
plotting.plot_general()
plotting.plot_quality()
