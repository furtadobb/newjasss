import parameters
from numpy import median

# General, population statistics
class Statistics(object):

    def __init__(self, price_index=None, GDP_index=1, out_workforce=0, employed=0, unemployment=0, families_wealth=0):
        self.price_index = price_index
        self.GDP_index = GDP_index
        self.out_workforce = out_workforce
        self.employed = employed
        self.unemployment = unemployment
        self.families_wealth = families_wealth

    def get_price(self):
        return self.price_index

    def get_GDP(self):
        return self.GDP_index

    def get_unemployment(self):
        return self.unemployment

    # Extract information from classes
    # Prices
    def average_price(self, firms):
        dummy_average_price = 0
        dummy_num_products = 0
        for firm in firms:
            for key in list(firm.inventory.keys()):
                dummy_average_price += firm.inventory[key].price
                dummy_num_products += 1
        if dummy_average_price == 0 or dummy_num_products == 0:
            average_price = 0
        else:
            average_price = dummy_average_price / dummy_num_products
        return average_price

    def update_price(self, firms):
        dummy_price = self.average_price(firms)
        self.price_index = dummy_price
        print('Price index Perc. Base Month1: ', self.price_index)
        return self.price_index

    # GDP based on FIRMS' revenues
    def calculate_GDP(self, firms):
        # Added value for all firms in a given period
        dummy_GDP = 0
        for firm in firms:
            dummy_GDP += firm.get_sold()
        return dummy_GDP

    def update_GDP(self, firms):
        dummy_gdp = self.calculate_GDP(firms)
        self.GDP_index = dummy_gdp
        print('GDP index: ', self.GDP_index)
        return self.GDP_index

    def update_GDP_capita(self, firms, regions):
        gdp_region_capita = []
        for region in regions:
            dummy_gdp = 0
            for firm in firms:
                if firm.get_region_id() == region.get_region():
                    dummy_gdp += firm.get_sold()
            if region.get_pop() > 0:
                dummy_gdp_capita = dummy_gdp / region.get_pop()
            else:
                dummy_gdp_capita = dummy_gdp
            gdp_region_capita.append(dummy_gdp_capita)
        return gdp_region_capita

    # Unemployment
    def calculate_unemployment(self, agents):
        dummy_out_workforce = 0
        dummy_employed = 0
        dummy_unemployed = 0
        for agent in agents:
            if agent.is_minor() or agent.is_retired():
                dummy_out_workforce += 1
            else:
                if agent.is_employed():
                    dummy_employed += 1
                else:
                    dummy_unemployed += 1
        self.out_workforce = dummy_out_workforce
        self.employed = dummy_employed
        dummy_temp = (dummy_unemployed / (dummy_unemployed + dummy_employed)) * 100
        self.unemployment = dummy_temp

    def update_unemployment(self, agents):
        self.calculate_unemployment(agents)
        print('Unemployment rate: ', self.unemployment)
        return self.unemployment

    def calculate_average_workers(self, firms):
        dummy_avg_workers = 0
        for firm in firms:
            dummy_avg_workers += len(firm.employees)

        return dummy_avg_workers / len(firms)

    # Calculate wealth: families, firms and profits

    def calculate_families_median_wealth(self, families):
        list = []
        for family in families:
            list.append(family.sum_balance())
        return median(list)

    def calculate_families_wealth(self, families):
        dummy_wealth = 0
        for family in families:
            dummy_wealth += family.sum_balance()
        return dummy_wealth

    def calculate_firms_wealth(self, firms):
        dummy_wealth = 0
        for firm in firms:
            dummy_wealth += firm.get_total_balance()
        return dummy_wealth

    def calculate_firms_profit(self, firms):
        dummy_profit = 0
        for firm in firms:
            dummy_profit += firm.get_profit()
        return dummy_profit

    # Calculate inequality (GINI)

    def calculate_utility(self, families):
        dummy_utility = 0
        dummy_families = 0
        for family in families:
            if family.num_members() > 0:
                dummy_utility += family.average_utility()
                dummy_families += 1
        return dummy_utility / dummy_families


    def calculate_GINI(self, families):
        family_data = []
        for family in families:
            if family.num_members() > 0:
                family_data.append(family.average_utility())
        n = len(family_data)
        assert(n > 0), 'Empty list of values'
        # Sort smallest to largest
        sorted_values = sorted(family_data)
        # Find cumulative totals
        cumm = [0]
        for i in range(n):
            cumm.append(sum(sorted_values[0:(i + 1)]))

        # Calculate Lorenz points
        LorenzPoints = [[], []]
        # Some of all y values
        sumYs = 0
        # Robin Hood index max(x_i, y_i)
        robinHoodIdx = -1
        for i in range(1, n + 2):
            x = 100.0 * (i - 1)/n
            y = 100.0 * (cumm[i - 1]/float(cumm[n]))
            LorenzPoints[0].append(x)
            LorenzPoints[1].append(y)
            sumYs += y
            maxX_Y = x - y
            if maxX_Y > robinHoodIdx: robinHoodIdx = maxX_Y

        # Gini index
        giniIdx = 100 + (100 - 2 * sumYs)/n
        # Possible to return [giniIdx, giniIdx/100, robinHoodIdx, LorenzPoints]
        # Returning exclusively GINI COEFFICIENT and Lorenz curve points
        print('GINI: ', giniIdx/100)
        return giniIdx/100
