import glob
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

try:
    plt.style.use('ggplot')
except AttributeError:
    pass


def plots_sensitivity_analysis():

    # Define the general output path to be used to create the results
    output_path_sensitivity = open("FilesforControl/output_path_set.txt").read().replace('\n', '')

    # create the variables as paths to save and process data
    output_path_plots = os.path.join(output_path_sensitivity, 'Plots/')

    # load the value of index loop (first loop that controls the selection of each parameter
    loop_index = int(([(np.loadtxt('FilesforControl/loop_index.txt'))])[0])

    # Load the file of parameters variation and limits to control the plots of sensitivity analysis
    dat_lim_run = pd.read_csv("InputData/parameters_limits_process.csv", sep=";", decimal=",", header=None)

    # create a sequence of parameter tested values to control all plot process
    parameters_sequence = np.linspace(float(dat_lim_run.iloc[loop_index][1]),
                                      float(dat_lim_run.iloc[loop_index][2]),
                                      num=int(dat_lim_run.iloc[loop_index][4]), endpoint=True)

    parameters_sequence = [float(format(t, '.2f')) for t in parameters_sequence]

    # time to be eliminated
    time_to_be_eliminated = 0

    # GENERAL DATA!!!
    # load the configuration of values to make the name of plots
    # parameters_names = [(np.loadtxt('C:/ABM_f2/Results/Sensitivity_analysis/parameters_names.txt'))]
    list_general_data = glob.glob(os.path.join(output_path_sensitivity,dat_lim_run.iloc[loop_index][0], "temp_general_*.txt"))
    dat = pd.read_csv(list_general_data[0],  sep=" ", decimal=",", header=None).iloc[:,0:10]
    dat.columns = ['month', 'price_index', 'gdp_index', 'unemployment', 'average_workers',
                   'families_wealth', 'firms_wealth', 'firms_profit', 'gini_index', 'average_utility']

    # time to be burned
    if time_to_be_eliminated > 0:
        dat = dat.loc[(dat['month']).astype(int) >= ((dat['month']).max() * time_to_be_eliminated), :]

    dat.index = list(range(0, dat.shape[0]))

    temp = pd.DataFrame([parameters_sequence[0]] * dat.shape[0])
    temp.columns = ['prm_val']
    dat = pd.concat([temp, dat], axis=1)

    for i in range(1, len(list_general_data)):
        dat_temp = pd.read_csv(list_general_data[i],  sep=" ", decimal=",", header=None).iloc[:,0:10]
        dat_temp.columns = ['month', 'price_index', 'gdp_index', 'unemployment', 'average_workers',
                   'families_wealth', 'firms_wealth', 'firms_profit', 'gini_index', 'average_utility']
        # time to be burned
        if time_to_be_eliminated > 0:
            dat_temp = dat_temp.loc[(dat_temp['month']).astype(int) >= ((dat_temp['month']).max() * time_to_be_eliminated), :]
        dat_temp.index = list(range(0, dat_temp.shape[0]))
        temp = pd.DataFrame([parameters_sequence[i]] * dat_temp.shape[0])
        temp.columns = ['prm_val']


        dat_temp = pd.concat([temp, dat_temp], axis=1)
        dat = pd.concat([dat, dat_temp], axis=0)
    percentage_of_pop = 1000
    # create a variable with the parameter tested
    parameter_tested = dat_lim_run.iloc[loop_index][0]

    # variable to add in the plot title
    title_parameter_name = dat_lim_run.iloc[loop_index][0].replace("_", " ").title()
    title_pop_val = 1000

    # Formatting the list of values for X axis
    list_of_years_division = list(range(dat['month'].min(), int(dat['month'].max()), 12))+[dat['month'].max()+1]
    list_of_years = [int(i/12) for i in list_of_years_division]

    # graph paramter variables
    y_var_breaks = 11
    dpi_var_plot = 700
    width_var_plot = 15
    height_var_plot = 10

    # plotting PRICE INDEX
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='price_index').astype(float)
    plot_data = dat_reformatted.plot(title='Price index : %s \nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values in %')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('Price_index_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi = dpi_var_plot)

    # plotting GDP VALUES
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='gdp_index').astype(float)
    plot_data = dat_reformatted.plot(title='GDP absolute values : %s\nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values in $')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('GDP_vals_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi = dpi_var_plot)

    # plotting UNEMPLOYMENT
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='unemployment').astype(float)
    plot_data = dat_reformatted.plot(title='Unemployment : %s\nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values in %')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('Unemployment_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi=dpi_var_plot)

    # plotting the AVERAGE OF WORKERS
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='average_workers').astype(float)
    plot_data = dat_reformatted.plot(title='Average of workers in the region : %s\nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Number of employees by firm')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('average_workers_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi=dpi_var_plot)

    # plotting the FAMILIES WEALTH
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='families_wealth').astype(float)
    plot_data = dat_reformatted.plot(title='Average of values of families wealth : %s\nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values in $')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('families_wealth_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi=dpi_var_plot)

    # plotting the FIRMS WEALTH
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='firms_wealth').astype(float)
    plot_data = dat_reformatted.plot(title='Average of values of firms wealth : %s\nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values in $')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('firms_wealth_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi=dpi_var_plot)

    # plotting the FIRMS PROFIT
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='firms_profit').astype(float)
    plot_data = dat_reformatted.plot(title='Average of values of firms profit : %s\nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values in $')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('firms_profit_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi=dpi_var_plot)

    # plotting GINI INDEX
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='gini_index').astype(float)
    plot_data = dat_reformatted.plot(title='GINI index values : %s\nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('GINI_index_%s_pop_%s.png' %
                                                 (parameter_tested, percentage_of_pop))), dpi=dpi_var_plot)

    # plotting the AVERAGE OF UTILITY
    dat_reformatted = dat.pivot(index='month', columns='prm_val', values='average_utility').astype(float)
    plot_data = dat_reformatted.plot(title='Average of utility by agents : %s \nNumber of agents : %s' %
                                           (title_parameter_name , title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')
    plot_data.set_ylabel('Values')
    plot_data.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plot_data.set_xticks(list_of_years_division)
    plot_data.set_xticklabels(list_of_years)
    plot_data.set_axis_bgcolor('w')
    fig = plot_data.get_figure()
    fig.set_size_inches(width_var_plot, height_var_plot)
    fig.savefig(os.path.join(output_path_plots, ('average_utility_%s_pop_%s.png' %
                                                 (parameter_tested,  percentage_of_pop))), dpi=dpi_var_plot)