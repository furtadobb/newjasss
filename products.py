__author__ = 'B. Furtado'

class Product(object):
        def __init__(self, product_id, quantity, price):
            self.product_id = product_id
            self.quantity = quantity
            self.price = price

        def get_product_id(self):
            return self.product_id

        def get_quantity(self):
            return self.quantity

        def __str__(self):
            return 'Product ID: %d, Quantity: %d, Price: %.1f' % (self.product_id, self.quantity, self.price)