
import random
import parameters
import communications
import output
import housing_market

# Creates board instance for communications and labor market procedures
my_journal = communications.Posting()

# Main sequential class that distributes de time events and call correspondingly functions
class TimeControl:

    # Controlling time
    # This module includes ACTIVITIES that happen periodically
    # Include OUTPUTs

    # Basics of time

    def __init__(self, days=0, months=0, quarters=0, years=0):
        self.days = days
        self.months = months
        self.quarters = quarters
        self.years = years

    # calculating time
    def get_time(self):
        return self.days, self.months, self.quarters, self.years

    # updating
    def update_days(self):
        self.days += 1

    def update_months(self):
        self.months += 1

    def update_quarters(self):
        self.quarters += 1

    def update_years(self):
        self.years += 1

    # Running activities

    def running_day(self, firms, agents, houses):

        # Initiating simulation ######################################################################################
        # Setup procedures
        if self.days == 0:
            # Setup of the simulation
            # beginning of a month, generate first product
            for firm in firms:
                firm.create_product()

            # First jobs allocated
            hire_fire(firms)
            look_for_jobs(agents)
            my_journal.assign_post()

            # Set region, produce initial quantity
            for firm in firms:
                firm.update_product_quantity()

            output.call_parameters()
            print('Initial hiring and products development completed')
            print('Simulation starts.')
            print('')

        # insert days activities here ################################################################################
        # Regular production. Everyday activities

        for firm in firms:
            firm.update_product_quantity()

        # Update day
        self.update_days()

    def running_month(self, firms, agents, families, houses, regions):

        # Insert monthly activities here #############################################################################
        # Save month for statistics purpose
        actual_month = self.months
        # FIRMS
        # Monthly payment

        for firm in firms:
            firm.make_payment()

        # GOVERNMENT
        # Tax firms when doing sales
        # Equalize money within family members
        equalize(families)
        consume(families, firms, regions)

        # Using taxes to improve public services
        for region in regions:
            region.calculate_pop(families)
            region.update_index()
            # Save parameters
            region_i = region.get_region()
            region_x = region.get_index()
            output.call_quality(actual_month, region_i, region_x)

        # Update profits
        # Check if necessary to update prices
        for firm in firms:
            firm.calculate_profit()
            firm.update_prices()

        # AGENTS
        look_for_jobs(agents)

        # FIRMS
        # Check if new employee needed (functions below)
        # Check if firing is necessary
        hire_fire(firms)

        # Matching
        my_journal.assign_post()

        # Process housing market
        housing_market.allocate_houses(families, houses, regions)

        # Pass monthly information to be stored in Statistics
        # Get month to pass onto output
        output.call_monthly_parameters(actual_month, firms, houses, agents, families)
        # Getting evolution of agents' money balance in months!
        output.call_statistics(actual_month, firms, agents, families, regions)
        output.call_gdp_capita(actual_month, firms, regions)

        # Update month
        self.update_months()
        print('Month(s): ',self.months)

    def running_quarters(self, firms):

        # Insert quarters activities here ############################################################################
        for firm in firms:
            firm.set_last_qtr()

        self.update_quarters()
        print('Quarter(s): ', self.quarters)

    def running_years(self, firms):

        self.update_years()
        print('Year(s): ', self.years)

    def __str__(self):
        return '%.1d year(s), %.1d quarter(s), ' \
               '%.1d month(s), %.1d, day(s)' % (self.years, self.quarters, self.months, self.days)

#######################################################################################################################
# Accessory functions
# Functions to call iteration of elements within firm from time activity
# Updating monthly firm procedures

def hire_fire(firms):
    for firm in firms:
        strategy = parameters.fixed_seed.random()
        if strategy > parameters.LABOUR_MARKET:
            if firm.profit > 0 or firm.get_total_balance() > 0 or firm.num_employees() == 0:
                my_journal.add_post(firm)
            else:
                firm.fire()

# Agent setup for hiring process
def look_for_jobs(agents):
    for agent in agents:
        if agent.is_employed() is False:
            if agent.is_minor() is False:
                if agent.is_retired() is False:
                    my_journal.add_candidate(agent)

# Consumption of the families
def equalize(families):
    for family in families:
        if family.num_members() > 0:
            amount = family.sum_balance()
            per_member = amount / family.num_members()
            family.update_balance(per_member)

def consume(families, firms, regions):
    for family in families:
        for key in list(family.members.keys()):
            family.members[key].consume(firms, regions)

