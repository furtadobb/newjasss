import parameters

class Posting(object):

    def __init__(self):
        self.available_postings = {}
        self.candidates = {}

    def add_post(self, firm):
        self.available_postings[firm.get_firm_id()] = firm

    def add_candidate(self, agent):
        self.candidates[agent.get_id()] = agent

    def assign_post(self):

        # Rank positions by wage and rank employees by qualifications
        # Make a match
        while len(self.candidates) > 0 and len(self.available_postings) > 0:

            # Higher wage
            key1 = parameters.fixed_seed.choice(list(self.available_postings.keys()))
            for key2 in list(self.available_postings.keys()):
                if self.available_postings[key2].set_wage_base() > self.available_postings[key1].set_wage_base():
                    dummy_higher = self.available_postings[key2]
                else:
                    dummy_higher = self.available_postings[key1]

            # Best qualification
            key3 = parameters.fixed_seed.choice(list(self.candidates.keys()))
            for key4 in list(self.candidates.keys()):
                if self.candidates[key4].get_qual() > self.candidates[key3].get_qual():
                    dummy_best = self.candidates[key4]
                else:
                    dummy_best = self.candidates[key3]

            # Closest distance. Check from candidates' house to all available posts, return closest candidate
            key5 = parameters.fixed_seed.choice(list(self.candidates.keys()))
            for key6 in list(self.candidates.keys()):
                for key7 in list(self.available_postings.keys()):
                    if self.candidates[key6].calculate_distance(self.available_postings[key7].address_x,
                                                                self.available_postings[key7].address_y) > \
                            self.candidates[key5].calculate_distance(self.available_postings[key7].address_x,
                                                                     self.available_postings[key7].address_y):
                        dummy_closest = self.candidates[key5]
                    else:
                        dummy_closest = self.candidates[key6]

            # Chose betweeen best qualification and closest distance
            chosen = parameters.fixed_seed.choice([dummy_closest, dummy_best])

            # Assignment. Firm has method add_employee
            dummy_higher.add_employee(chosen)
            # Remove agent and firm from list
            del self.available_postings[dummy_higher.get_firm_id()]
            del self.candidates[chosen.get_id()]

    def __str__(self):
        return self.available_postings, self.candidates
