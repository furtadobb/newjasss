import parameters
import random
import households
import agents
import time_iteration
import firms
import families
import government
import SPACE
import output

# Generating REGIONS ##################################################################################################
def create_regions():
    region0 = government.Govern(0, SPACE.REGION0)
    region1 = government.Govern(1, SPACE.REGION1)
    region2 = government.Govern(2, SPACE.REGION2)
    region3 = government.Govern(3, SPACE.REGION3)
    regions = [region0, region1, region2, region3]
    return regions

def create_uno():
    region0 = [government.Govern(0, SPACE.REGION_UNO)]
    return region0

def create_seven():
    region0 = government.Govern(0, SPACE.REGION0)
    region1 = government.Govern(1, SPACE.REGION1)
    region2 = government.Govern(2, SPACE.REGION2)
    region3 = government.Govern(3, SPACE.REGION_3)
    region4 = government.Govern(4, SPACE.REGION_4)
    region5 = government.Govern(5, SPACE.REGION_5)
    region6 = government.Govern(6, SPACE.REGION_6)
    regions = [region0, region1, region2, region3, region4, region5, region6]
    return regions

# Generating Agents ##################################################################################################
def create_agent():
    dummy_pop = []
    for dummy_agent in range(parameters.TOTAL_AGENTS):
        age = parameters.fixed_seed.randrange(0, 76)
        qualification = parameters.fixed_seed.randrange(1, 20)
        if age < 6:
            qualification = 0
        elif age < 26:
            qualification = min(age - 6, qualification)
        money = parameters.fixed_seed.randrange(20, 200)
        dummy_pop.append(agents.Agent(dummy_agent, age, qualification, money))
    return dummy_pop

# Generating families #################################################################################################
def create_family():
    dummy_community = []
    for dummy_family in range(parameters.TOTAL_FAMILIES):
        dummy_community.append(families.Family(dummy_family))
    return dummy_community

# Generating households ##############################################################################################
def create_household():
    dummy_neighborhood = []
    for dummy_house in range(parameters.TOTAL_HOUSEHOLDS):
        address_x = parameters.fixed_seed.uniform(SPACE.SPACE_WEST, SPACE.SPACE_EAST)
        address_y = parameters.fixed_seed.uniform(SPACE.SPACE_SOUTH, SPACE.SPACE_NORTH)
        size = parameters.fixed_seed.randrange(20, 120)
        price = (parameters.fixed_seed.randrange(5, 50) * size)
        dummy_neighborhood.append(households.Household(dummy_house, address_x, address_y, size, price))
    return dummy_neighborhood

# Generating firms ####################################################################################################
def create_firm():
    dummy_sector = []
    for dummy_firm in range(parameters.TOTAL_FIRMS):
        address_x = parameters.fixed_seed.uniform(SPACE.SPACE_WEST, SPACE.SPACE_EAST)
        address_y = parameters.fixed_seed.uniform(SPACE.SPACE_SOUTH, SPACE.SPACE_NORTH)
        total_balance = parameters.fixed_seed.randrange(20, 400)
        dummy_sector.append(firms.Firm(dummy_firm, address_x, address_y, total_balance))
    return dummy_sector

# Controlling time ####################################################################################################
def have_a_go(firms, agents, families, houses, regions):

    # iterate through days, months, quarters, years and finish
    while my_simulation.days < parameters.TOTAL_DAYS:
        my_simulation.running_day(firms, agents, houses)
        if my_simulation.days % 21 == 0:
            my_simulation.running_month(firms, agents, families, houses, regions)
        if my_simulation.days % 63 == 0:
            my_simulation.running_quarters(firms)
        if my_simulation.days % 252 == 0:
            my_simulation.running_years(firms)
    print(my_simulation)
    print('')
    for region in regions:
        print(region)
    output.region_index(regions)
    print(my_simulation)
    print('')
    print('Simulation completed.')
    print('')

# Initializing my_simulation
my_simulation = time_iteration.TimeControl()

