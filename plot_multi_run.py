import glob
import os

import matplotlib.pyplot as plt
import numpy as np

import pandas as pd

try:
    plt.style.use('ggplot')
except AttributeError:
    pass


def general_multi_run_analysis():

    # Defining the variable to add to the plots names
    percentage_of_pop = 1000

    # Define the general output path to be used to create the results
    output_path_multirun = str(open("FilesforControl/output_path_set.txt").read().replace('\n', ''))

    # create the variables as paths to save and process data
    output_path_plots = os.path.join(output_path_multirun, 'Plots')

    # load the number of loops to process the multi run simulation
    number_of_loops = int(([(np.loadtxt('FilesforControl/number_of_runs.txt'))])[0])

    # time to be eliminated
    time_to_be_eliminated = 0

    # GENERAL DATA!!!
    # load the configuration of values to make the name of plots
    # parameters_names = [(np.loadtxt('C:/ABM_f2/Results/Sensitivity_analysis/parameters_names.txt'))]

    general_data_list = glob.glob(os.path.join(output_path_multirun, '*_general_*.txt'))

    dat = pd.read_csv(general_data_list[0],
                      sep=" ", decimal=",", header=None).apply(pd.to_numeric, errors='coerce').iloc[:,0:10]

    dat.columns = ['month', 'price_index', 'gdp_index', 'unemployment', 'average_workers',
                   'families_wealth', 'firms_wealth', 'firms_profit', 'gini_index', 'average_utility']

    # time to be burned
    if time_to_be_eliminated > 0:
        dat = dat.loc[(dat['month']).astype(int) >= ((dat['month']).max() * time_to_be_eliminated), :]

    dat.index = list(range(0, dat.shape[0]))

    temp = pd.DataFrame([0] * dat.shape[0])

    temp.columns = ['loop']

    temp.index = list(range(0, temp.shape[0]))

    dat = pd.concat([temp, dat], axis=1)

    for i in range(1, len(general_data_list)):

        dat_temp = pd.read_csv(general_data_list[i],  sep=" ", decimal=",",
                               header=None).apply(pd.to_numeric, errors='coerce').iloc[:,0:10]

        dat_temp.columns = ['month', 'price_index', 'gdp_index', 'unemployment', 'average_workers',
                            'families_wealth', 'firms_wealth', 'firms_profit', 'gini_index', 'average_utility']

        # time to be burned
        if time_to_be_eliminated > 0:

            dat_temp = dat_temp.loc[(dat_temp['month']).astype(int) >= ((dat_temp['month']).max() *
                                                                        time_to_be_eliminated), :]

        dat_temp.index = list(range(0, dat_temp.shape[0]))

        temp = pd.DataFrame([i] * dat_temp.shape[0])

        temp.columns = ['loop']

        dat_temp = pd.concat([temp, dat_temp], axis=1)

        dat = pd.concat([dat, dat_temp], axis=0)

    # create a list of a years to plot
    list_of_years_division = list(range(dat['month'].min(), int(dat['month'].max()), 12))+[dat['month'].max()+1]

    list_of_years = [int(i/12) for i in list_of_years_division]

    # graph paramter variables
    dpi_var_plot = 700

    width_var_plot = 15

    height_var_plot = 10

    # variable to add in the plot title
    title_pop_val = int(percentage_of_pop)

    # plotting QUALITY
    dat_to_plot = dat.pivot(index='month', columns='loop', values='price_index').astype(float)

    plot_data = dat_to_plot.plot(legend=None, title='Price index \nNumber of runs : %s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values in %')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('Price_index_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting GDP VALUES
    dat_to_plot = dat.pivot(index='month', columns='loop', values='gdp_index').astype(float)

    plot_data = dat_to_plot.plot(legend=None, title='GDP absolute values \nNumber of runs : %s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values in $')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('GDP_vals_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting UNEMPLOYMENT
    dat_to_plot = dat.pivot(index='month', columns='loop', values='unemployment').astype(float)

    plot_data = dat_to_plot.plot(legend=None, title='Unemployment \nNumber of runs : %s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')

    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values in %')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('Unemployment_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting the AVERAGE OF WORKERS
    dat_to_plot = dat.pivot(index='month', columns='loop', values='average_workers').astype(float)

    plot_data = dat_to_plot.plot(legend=None,
                                 title='Average of workers in the region \nNumber of runs :%s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')

    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Number of employees by firm')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('average_workers_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting the FAMILIES WEALTH
    dat_to_plot = dat.pivot(index='month', columns='loop', values='families_wealth').astype(float)

    plot_data = dat_to_plot.plot(legend=None, title='Average of values of families wealth \nNumber of runs : %s '
                                                    '\nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')

    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values in $')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('families_wealth_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting the FIRMS WEALTH
    dat_to_plot = dat.pivot(index='month', columns='loop', values='firms_wealth').astype(float)

    plot_data = dat_to_plot.plot(legend=None,
                                 title='Average of values of firms wealth \nNumber of runs : %s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')

    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values in $')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('firms_wealth_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting the FIRMS PROFIT
    dat_to_plot = dat.pivot(index='month', columns='loop', values='firms_profit').astype(float)

    plot_data = dat_to_plot.plot(legend=None,
                                 title='Average of values of firms profit \nNumber of runs : %s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')

    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values in $')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('firms_profit_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting GINI INDEX
    dat_to_plot = dat.pivot(index='month', columns='loop', values='gini_index').astype(float)

    plot_data = dat_to_plot.plot(legend=None, title='GINI index values \nNumber of runs : %s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')
    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('GINI_index_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # plotting the AVERAGE OF UTILITY
    dat_to_plot = dat.pivot(index='month', columns='loop', values='average_utility').astype(float)

    plot_data = dat_to_plot.plot(legend=None, title='Average of utility by agents \nNumber of runs : %s \nNumber of Agents : %s' %
                                                    (number_of_loops, title_pop_val), colormap='Spectral')

    plot_data.set_xlabel('Years')

    plot_data.set_ylabel('Values')

    plot_data.set_xticks(list_of_years_division)

    plot_data.set_xticklabels(list_of_years)

    plot_data.set_axis_bgcolor('w')

    fig = plot_data.get_figure()

    fig.set_size_inches(width_var_plot, height_var_plot)

    fig.savefig(os.path.join(output_path_plots, ('average_utility_%s.png' % percentage_of_pop)), dpi=dpi_var_plot)

    # Saving the descriptive stats for general data
    # selecting the last month to compare
    dat_last_month = dat[dat['month'] == dat['month'].max()]

    # excluding loop and month columns
    dat_last_month = dat_last_month.iloc[:, dat_last_month.columns != 'loop']

    dat_last_month = dat_last_month.iloc[:, dat_last_month.columns != 'month']

    # generating the descriptive stats, mean, standard deviation, median, percentiles 25% and 75% for each variable
    general_descriptives_stats = pd.concat([pd.DataFrame(dat_last_month.mean(), columns=['mean']),
                                            pd.DataFrame(dat_last_month.std(), columns=['std']),
                                            pd.DataFrame(dat_last_month.median(), columns=['median']),
                                            pd.DataFrame(dat_last_month.quantile(q=0.25), columns=['p_25']),
                                            pd.DataFrame(dat_last_month.quantile(q=0.75), columns=['p_75'])], axis=1)

    # saving the descriptive stats for general data
    general_descriptives_stats.to_csv(os.path.join(output_path_plots,
                                ("general_descriptives_stats_%s_pop_%s_number_of_runs.csv" %
                                (number_of_loops, percentage_of_pop ))), sep=";", decimal=",", header=True)
