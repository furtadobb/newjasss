import os
from timeit import default_timer as timer

import numpy as np
import pandas as pd
import plot_sensitivity_analysis

# First variable to control the time test for all process
start_time_overall = timer()

#######################################################################################################################
#### setting the parameters data archive to run the model
# Improving the data.frame with the variable names, minimum and maximum values to generate the sequences
dat_temp = pd.read_csv("InputData/parameters_limits.csv", sep=";", decimal=",", header=0)

# defining the number of divisions for each parameter sequence
number_of_test_parameter_values = 10

# merging the parameter data to number of divisions
dat_lim_run = dat_temp.join((pd.DataFrame([number_of_test_parameter_values] * dat_temp.shape[0])), how='inner')

# changing the names of columns
dat_lim_run.columns = ['vars', 'min', 'max', 'default', 'number_of_repetions']

# saving the new dataframe as the number of simulations for each parameter
dat_lim_run.to_csv("InputData/parameters_limits_process.csv", sep=";", decimal=",", header=0, index=None)
########################################################################################################################

# Creating the function to save the loop index "i" for use that in "plot_sensitivity_analysis.py" module
def save_first_loop():
    if os.path.isfile("FilesforControl/loop_index.txt") is True:
        os.remove("FilesforControl/loop_index.txt")
    parameters_txt = open("FilesforControl/loop_index.txt", 'a')
    parameters_txt.write('%d' % i)
    parameters_txt.close()

# Loop to create the sequence of values to test in the model
# First loop is to run over the number of rows of data.frame. Each row is one parameter
# Second loop is to define the value of parameter to be used.
# The sequence of values are defined between the values in the data.frame

for i in range(dat_lim_run.shape[0]):
    start_time_parameter = timer()
    # Saving the "i" loop index to process the data in plot analysis
    save_first_loop()

    # Creating the list of values for each parameter
    parameters_sequence = np.linspace(float(dat_lim_run.iloc[i][1]),float(dat_lim_run.iloc[i][2]),
                                      num=int(dat_lim_run.iloc[i][4]), endpoint=True)
    # round the
    parameters_sequence = [float(format(t, '.2f')) for t in parameters_sequence]

    # Loop to control the main.py module to run for each parameter variation defined by "parameter_sequence"
    for prm_val in parameters_sequence:
        # Call main.py to run one round simulation
        # Building-up the string to call the main.py module
        cmd = "python main.py " + str(i) + " " + str(prm_val)
        # Actually all the main model using system
        os.system(cmd)

    # call the module to produce the data analysis for plot by each parameter tested
    plot_sensitivity_analysis.plots_sensitivity_analysis()
    # variable to end the time to create the agents
    end_time_parameter = timer()

    # Logical test to define the elapsed time based on the time to define the unit
    m, s = divmod((end_time_parameter-start_time_parameter), 60)
    h, m = divmod(m, 60)

    print('')
    print('-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-')
    print("Elapsed time to one parameter %d hs %02d min %02d sec" % (h, m, s))
    print('-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-')


# variable to end the time to create the agents
end_time_overall = timer()

# Logical test to define the elapsed time based on the time to define the unit
m, s = divmod((end_time_overall-start_time_overall), 60)
h, m = divmod(m, 60)

print('')
print('###################################################')
print('---------------------------------------------------')
print("Elapsed time to test all parameters %d hs %02d min %02d sec" % (h, m, s))
print('---------------------------------------------------')
print('###################################################')