
import random
import parameters
import math

class Agent(object):

    # Class for agents. Citizens of the model
    # Agents live in families, work in firms, consume
    def __init__(self, id, age, qualification, money, firm_id=None, family_id=None, utility=1,
                 address_x=None, address_y=None):

        self.id = id
        self.age = age
        self.qualification = qualification
        self.money = money
        self.firm_id = firm_id
        self.family_id = family_id
        self.utility = utility
        self.address_x = address_x
        self.address_y = address_y

    def get_id(self):
        return self.id

    def get_age(self):
        return self.age

    def is_minor(self):
        if self.age < 16:
            return True
        else:
            return False

    def is_retired(self):
        if self.age > 70:
            return True
        else:
            return False

    def get_qual(self):
        return self.qualification

    def get_money(self):
        return self.money

    def update_money(self, amount):
        # Agent may have less or more money per capita in the family,
        # New amount is the average of the family
        self.money = amount

    # Family ##########################################################################################################
    def set_family(self, family_id):
        self.family_id = family_id

    def get_family_id(self):
        return self.family_id

    def belongs_to_family(self):
        if self.family_id is None:
            return False
        else:
            return True

    def set_address(self, address):
        self.address_x = address[0]
        self.address_y = address[1]

    def get_address(self):
        return self.address_x, self.address_y

    # Workplace #######################################################################################################

    def set_workplace(self, firm_id):
        self.firm_id = firm_id

    def is_employed(self):
        if self.firm_id is None:
            return False
        else:
            return True

    def get_workplace(self):
        return self.firm_id

    # Consumption ####################################################################################################

    def consume(self, firms, regions):

        if self.money > 0:
            if self.money < 1:
                dummy_money = parameters.fixed_seed.uniform(0, self.money)
            else:
                dummy_money = parameters.fixed_seed.uniform(0, (self.money ** parameters.BETA))

            # Picks SIZE_MARKET number of firms at random and choose the closest or the cheapest
            # Consumes from each product the chosen firm offers
            market = []
            for key in range(int(parameters.SIZE_MARKET)):
                market.append(parameters.fixed_seed.choice(firms))
            # Choose firm with cheapest average prices
            prices = []
            for firm in market:
                prices.append(firm.get_prices())
            cheapest = market[prices.index(min(prices))]

            # Choose closest firm
            distances = []
            for firm in market:
                d = self.calculate_distance(firm.address_x, firm.address_y)
                distances.append(d)
            closest = market[distances.index(min(distances))]

            # Choose randomly between cheapest or closest
            chosen_firm = parameters.fixed_seed.choice([cheapest, closest])

            # Buy from chosen company

            change = chosen_firm.sale(dummy_money, regions)
            self.money -= (dummy_money - change)
            # Rounding values
            if self.money < 0:
                self.money = 0
            # Utility
            self.utility += (parameters.CONSUMPTION_SATISFACTION * (dummy_money - change))

    # Utility #########################################################################################################
    def get_utility(self):
        return self.utility

    # Calculate distance from actual home
    # Calculate distance, every time it is needed to avoid extra calculation time

    def calculate_distance(self, x, y):
        distance = math.sqrt((x - self.address_x) ** 2 + (y - self.address_y) ** 2)
        return distance

    # Printing
    def __str__(self):
        return 'Ag. ID: %s, Qual. %s, Age: %s, $ %s, Firm: %s, Util. %s' % (self.id, self.qualification, self.age,
                                                                            self.money, self.firm_id, self.utility)